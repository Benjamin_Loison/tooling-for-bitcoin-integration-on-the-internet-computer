import hashlib

def sha256(data):
    data = data.encode('utf-8')
    return hashlib.sha256(data).hexdigest()

def sha256d(data):
    return sha256(sha256(data))

n = 0

while True:
    dataParts = [f'Block 3 ({n})', 'Reward pk_A 10 BTC']
    data = "\n".join(dataParts)
    hash = sha256d(data)
    if hash.startswith('00'):
        print(n)
        break
    n += 1

