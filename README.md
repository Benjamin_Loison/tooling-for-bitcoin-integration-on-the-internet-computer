# Tooling for Bitcoin Integration on the Internet Computer

Research internship that took place from February to July 2022 at the [DFINITY Foundation](https://en.wikipedia.org/wiki/Dfinity) offices in Zurich (Switzerland) under Thomas Locher supervision. He wrote me [this reference letter](https://gitlab.com/Benjamin_Loison/tooling-for-bitcoin-integration-on-the-internet-computer/-/blob/master/reference%20letter.jpg).

In the context of [the direct integration with Bitcoin for the Internet Computer](https://internetcomputer.org/bitcoin-integration/), after having designed efficient approaches related to Bitcoin, I notably implemented:

- [The Basic Motoko Bitcoin example](https://github.com/dfinity/examples/tree/master/motoko/basic_bitcoin) from [the Basic Rust Bitcoin example](https://github.com/dfinity/examples/tree/master/rust/basic_bitcoin)
- [Motoko and Rust Bitcoin libraries](https://github.com/Benjamin-Loison/Internet-Computer-Bitcoin-Library)
- [A Rust Bitcoin wallet](https://github.com/dfinity/examples/pull/321) using my Bitcoin library ([demonstration](https://www.youtube.com/watch?v=Lq6gM7JHPCs))

As the Internet Computer wrapped Bitcoin (ckBTC) relies on my Bitcoin library, I fully reviewed [ckBTC Rust code](https://github.com/dfinity/ic/tree/master/rs/bitcoin/ckbtc/minter) until I left the DFINITY Foundation on 28 July 2022.
I also verified its formal model written in PlusCal (unfortunately this document remains confidential and can’t be shared with the reader).

I have written [a report](https://gitlab.com/Benjamin_Loison/tooling-for-bitcoin-integration-on-the-internet-computer/-/raw/master/PDFs/report.pdf) and [slides](https://gitlab.com/Benjamin_Loison/tooling-for-bitcoin-integration-on-the-internet-computer/-/raw/master/PDFs/slides.pdf) to present my internship.

My DFINITY GitHub profile: [BenjaminLoison](https://github.com/BenjaminLoison)
